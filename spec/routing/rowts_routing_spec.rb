require "rails_helper"

RSpec.describe RowtsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/rowts").to route_to("rowts#index")
    end

    it "routes to #new" do
      expect(:get => "/rowts/new").to route_to("rowts#new")
    end

    it "routes to #show" do
      expect(:get => "/rowts/1").to route_to("rowts#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/rowts/1/edit").to route_to("rowts#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/rowts").to route_to("rowts#create")
    end

    it "routes to #update" do
      expect(:put => "/rowts/1").to route_to("rowts#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/rowts/1").to route_to("rowts#destroy", :id => "1")
    end

  end
end
