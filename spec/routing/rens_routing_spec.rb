require "rails_helper"

RSpec.describe RensController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/rens").to route_to("rens#index")
    end

    it "routes to #new" do
      expect(:get => "/rens/new").to route_to("rens#new")
    end

    it "routes to #show" do
      expect(:get => "/rens/1").to route_to("rens#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/rens/1/edit").to route_to("rens#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/rens").to route_to("rens#create")
    end

    it "routes to #update" do
      expect(:put => "/rens/1").to route_to("rens#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/rens/1").to route_to("rens#destroy", :id => "1")
    end

  end
end
