require "rails_helper"

RSpec.describe BonusController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/bonus").to route_to("bonus#index")
    end

    it "routes to #new" do
      expect(:get => "/bonus/new").to route_to("bonus#new")
    end

    it "routes to #show" do
      expect(:get => "/bonus/1").to route_to("bonus#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/bonus/1/edit").to route_to("bonus#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/bonus").to route_to("bonus#create")
    end

    it "routes to #update" do
      expect(:put => "/bonus/1").to route_to("bonus#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/bonus/1").to route_to("bonus#destroy", :id => "1")
    end

  end
end
