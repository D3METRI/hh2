require "rails_helper"

RSpec.describe SlocsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/slocs").to route_to("slocs#index")
    end

    it "routes to #new" do
      expect(:get => "/slocs/new").to route_to("slocs#new")
    end

    it "routes to #show" do
      expect(:get => "/slocs/1").to route_to("slocs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/slocs/1/edit").to route_to("slocs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/slocs").to route_to("slocs#create")
    end

    it "routes to #update" do
      expect(:put => "/slocs/1").to route_to("slocs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/slocs/1").to route_to("slocs#destroy", :id => "1")
    end

  end
end
