require 'rails_helper'

RSpec.describe "invoices/new", type: :view do
  before(:each) do
    assign(:invoice, Invoice.new(
      :name => "MyString",
      :amount => "9.99",
      :user => nil,
      :ren => nil,
      :stat => nil
    ))
  end

  it "renders new invoice form" do
    render

    assert_select "form[action=?][method=?]", invoices_path, "post" do

      assert_select "input#invoice_name[name=?]", "invoice[name]"

      assert_select "input#invoice_amount[name=?]", "invoice[amount]"

      assert_select "input#invoice_user_id[name=?]", "invoice[user_id]"

      assert_select "input#invoice_ren_id[name=?]", "invoice[ren_id]"

      assert_select "input#invoice_stat_id[name=?]", "invoice[stat_id]"
    end
  end
end
