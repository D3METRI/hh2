require 'rails_helper'

RSpec.describe "invoices/edit", type: :view do
  before(:each) do
    @invoice = assign(:invoice, Invoice.create!(
      :name => "MyString",
      :amount => "9.99",
      :user => nil,
      :ren => nil,
      :stat => nil
    ))
  end

  it "renders the edit invoice form" do
    render

    assert_select "form[action=?][method=?]", invoice_path(@invoice), "post" do

      assert_select "input#invoice_name[name=?]", "invoice[name]"

      assert_select "input#invoice_amount[name=?]", "invoice[amount]"

      assert_select "input#invoice_user_id[name=?]", "invoice[user_id]"

      assert_select "input#invoice_ren_id[name=?]", "invoice[ren_id]"

      assert_select "input#invoice_stat_id[name=?]", "invoice[stat_id]"
    end
  end
end
