require 'rails_helper'

RSpec.describe "rens/index", type: :view do
  before(:each) do
    assign(:rens, [
      Ren.create!(
        :step => nil,
        :user => nil,
        :visit => false,
        :rentim => ""
      ),
      Ren.create!(
        :step => nil,
        :user => nil,
        :visit => false,
        :rentim => ""
      )
    ])
  end

  it "renders a list of rens" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "".to_s, :count => 2
  end
end
