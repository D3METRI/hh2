require 'rails_helper'

RSpec.describe "rens/edit", type: :view do
  before(:each) do
    @ren = assign(:ren, Ren.create!(
      :step => nil,
      :user => nil,
      :visit => false,
      :rentim => ""
    ))
  end

  it "renders the edit ren form" do
    render

    assert_select "form[action=?][method=?]", ren_path(@ren), "post" do

      assert_select "input#ren_step_id[name=?]", "ren[step_id]"

      assert_select "input#ren_user_id[name=?]", "ren[user_id]"

      assert_select "input#ren_visit[name=?]", "ren[visit]"

      assert_select "input#ren_rentim[name=?]", "ren[rentim]"
    end
  end
end
