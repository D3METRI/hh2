require 'rails_helper'

RSpec.describe "stats/new", type: :view do
  before(:each) do
    assign(:stat, Stat.new(
      :name => "MyString",
      :amount => "9.99",
      :user => nil
    ))
  end

  it "renders new stat form" do
    render

    assert_select "form[action=?][method=?]", stats_path, "post" do

      assert_select "input#stat_name[name=?]", "stat[name]"

      assert_select "input#stat_amount[name=?]", "stat[amount]"

      assert_select "input#stat_user_id[name=?]", "stat[user_id]"
    end
  end
end
