require 'rails_helper'

RSpec.describe "stats/edit", type: :view do
  before(:each) do
    @stat = assign(:stat, Stat.create!(
      :name => "MyString",
      :amount => "9.99",
      :user => nil
    ))
  end

  it "renders the edit stat form" do
    render

    assert_select "form[action=?][method=?]", stat_path(@stat), "post" do

      assert_select "input#stat_name[name=?]", "stat[name]"

      assert_select "input#stat_amount[name=?]", "stat[amount]"

      assert_select "input#stat_user_id[name=?]", "stat[user_id]"
    end
  end
end
