require 'rails_helper'

RSpec.describe "lineitems/new", type: :view do
  before(:each) do
    assign(:lineitem, Lineitem.new(
      :invoice => nil,
      :stat => nil,
      :ren => nil
    ))
  end

  it "renders new lineitem form" do
    render

    assert_select "form[action=?][method=?]", lineitems_path, "post" do

      assert_select "input#lineitem_invoice_id[name=?]", "lineitem[invoice_id]"

      assert_select "input#lineitem_stat_id[name=?]", "lineitem[stat_id]"

      assert_select "input#lineitem_ren_id[name=?]", "lineitem[ren_id]"
    end
  end
end
