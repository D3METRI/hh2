require 'rails_helper'

RSpec.describe "lineitems/edit", type: :view do
  before(:each) do
    @lineitem = assign(:lineitem, Lineitem.create!(
      :invoice => nil,
      :stat => nil,
      :ren => nil
    ))
  end

  it "renders the edit lineitem form" do
    render

    assert_select "form[action=?][method=?]", lineitem_path(@lineitem), "post" do

      assert_select "input#lineitem_invoice_id[name=?]", "lineitem[invoice_id]"

      assert_select "input#lineitem_stat_id[name=?]", "lineitem[stat_id]"

      assert_select "input#lineitem_ren_id[name=?]", "lineitem[ren_id]"
    end
  end
end
