require 'rails_helper'

RSpec.describe "steps/index", type: :view do
  before(:each) do
    assign(:steps, [
      Step.create!(
        :name => "Name",
        :address => "MyText",
        :value => "9.99",
        :rowt_id => 1
      ),
      Step.create!(
        :name => "Name",
        :address => "MyText",
        :value => "9.99",
        :rowt_id => 1
      )
    ])
  end

  it "renders a list of steps" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
