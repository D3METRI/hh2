require 'rails_helper'

RSpec.describe "steps/edit", type: :view do
  before(:each) do
    @step = assign(:step, Step.create!(
      :name => "MyString",
      :address => "MyText",
      :value => "9.99",
      :rowt_id => 1
    ))
  end

  it "renders the edit step form" do
    render

    assert_select "form[action=?][method=?]", step_path(@step), "post" do

      assert_select "input#step_name[name=?]", "step[name]"

      assert_select "textarea#step_address[name=?]", "step[address]"

      assert_select "input#step_value[name=?]", "step[value]"

      assert_select "input#step_rowt_id[name=?]", "step[rowt_id]"
    end
  end
end
