require 'rails_helper'

RSpec.describe "slocs/edit", type: :view do
  before(:each) do
    @sloc = assign(:sloc, Sloc.create!(
      :officename => "MyString",
      :address => "MyString",
      :lat => 1.5,
      :lng => 1.5
    ))
  end

  it "renders the edit sloc form" do
    render

    assert_select "form[action=?][method=?]", sloc_path(@sloc), "post" do

      assert_select "input#sloc_officename[name=?]", "sloc[officename]"

      assert_select "input#sloc_address[name=?]", "sloc[address]"

      assert_select "input#sloc_lat[name=?]", "sloc[lat]"

      assert_select "input#sloc_lng[name=?]", "sloc[lng]"
    end
  end
end
