require 'rails_helper'

RSpec.describe "slocs/show", type: :view do
  before(:each) do
    @sloc = assign(:sloc, Sloc.create!(
      :officename => "Officename",
      :address => "Address",
      :lat => 1.5,
      :lng => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Officename/)
    expect(rendered).to match(/Address/)
    expect(rendered).to match(/1.5/)
    expect(rendered).to match(/1.5/)
  end
end
