require 'rails_helper'

RSpec.describe "slocs/index", type: :view do
  before(:each) do
    assign(:slocs, [
      Sloc.create!(
        :officename => "Officename",
        :address => "Address",
        :lat => 1.5,
        :lng => 1.5
      ),
      Sloc.create!(
        :officename => "Officename",
        :address => "Address",
        :lat => 1.5,
        :lng => 1.5
      )
    ])
  end

  it "renders a list of slocs" do
    render
    assert_select "tr>td", :text => "Officename".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
    assert_select "tr>td", :text => 1.5.to_s, :count => 2
  end
end
