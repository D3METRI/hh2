require 'rails_helper'

RSpec.describe "expenses/new", type: :view do
  before(:each) do
    assign(:expense, Expense.new(
      :name => "MyString",
      :amount => "9.99",
      :user => ""
    ))
  end

  it "renders new expense form" do
    render

    assert_select "form[action=?][method=?]", expenses_path, "post" do

      assert_select "input#expense_name[name=?]", "expense[name]"

      assert_select "input#expense_amount[name=?]", "expense[amount]"

      assert_select "input#expense_user[name=?]", "expense[user]"
    end
  end
end
