require 'rails_helper'

RSpec.describe "schedules/new", type: :view do
  before(:each) do
    assign(:schedule, Schedule.new(
      :day => "MyString",
      :user => nil,
      :rowt => nil
    ))
  end

  it "renders new schedule form" do
    render

    assert_select "form[action=?][method=?]", schedules_path, "post" do

      assert_select "input#schedule_day[name=?]", "schedule[day]"

      assert_select "input#schedule_user_id[name=?]", "schedule[user_id]"

      assert_select "input#schedule_rowt_id[name=?]", "schedule[rowt_id]"
    end
  end
end
