require 'rails_helper'

RSpec.describe "schedules/show", type: :view do
  before(:each) do
    @schedule = assign(:schedule, Schedule.create!(
      :day => "Day",
      :user => nil,
      :rowt => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Day/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
