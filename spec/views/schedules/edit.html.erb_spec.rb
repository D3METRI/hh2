require 'rails_helper'

RSpec.describe "schedules/edit", type: :view do
  before(:each) do
    @schedule = assign(:schedule, Schedule.create!(
      :day => "MyString",
      :user => nil,
      :rowt => nil
    ))
  end

  it "renders the edit schedule form" do
    render

    assert_select "form[action=?][method=?]", schedule_path(@schedule), "post" do

      assert_select "input#schedule_day[name=?]", "schedule[day]"

      assert_select "input#schedule_user_id[name=?]", "schedule[user_id]"

      assert_select "input#schedule_rowt_id[name=?]", "schedule[rowt_id]"
    end
  end
end
