require 'rails_helper'

RSpec.describe "locations/new", type: :view do
  before(:each) do
    assign(:location, Location.new(
      :address => "MyString",
      :lat => 1.5,
      :lng => 1.5,
      :step => nil
    ))
  end

  it "renders new location form" do
    render

    assert_select "form[action=?][method=?]", locations_path, "post" do

      assert_select "input#location_address[name=?]", "location[address]"

      assert_select "input#location_lat[name=?]", "location[lat]"

      assert_select "input#location_lng[name=?]", "location[lng]"

      assert_select "input#location_step_id[name=?]", "location[step_id]"
    end
  end
end
