require 'rails_helper'

RSpec.describe "bonus/edit", type: :view do
  before(:each) do
    @bonu = assign(:bonu, Bonu.create!(
      :amount => "9.99",
      :reason => "MyString",
      :user => nil
    ))
  end

  it "renders the edit bonu form" do
    render

    assert_select "form[action=?][method=?]", bonu_path(@bonu), "post" do

      assert_select "input#bonu_amount[name=?]", "bonu[amount]"

      assert_select "input#bonu_reason[name=?]", "bonu[reason]"

      assert_select "input#bonu_user_id[name=?]", "bonu[user_id]"
    end
  end
end
