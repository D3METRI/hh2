require 'rails_helper'

RSpec.describe "bonus/new", type: :view do
  before(:each) do
    assign(:bonu, Bonu.new(
      :amount => "9.99",
      :reason => "MyString",
      :user => nil
    ))
  end

  it "renders new bonu form" do
    render

    assert_select "form[action=?][method=?]", bonus_path, "post" do

      assert_select "input#bonu_amount[name=?]", "bonu[amount]"

      assert_select "input#bonu_reason[name=?]", "bonu[reason]"

      assert_select "input#bonu_user_id[name=?]", "bonu[user_id]"
    end
  end
end
