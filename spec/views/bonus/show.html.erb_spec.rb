require 'rails_helper'

RSpec.describe "bonus/show", type: :view do
  before(:each) do
    @bonu = assign(:bonu, Bonu.create!(
      :amount => "9.99",
      :reason => "Reason",
      :user => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/Reason/)
    expect(rendered).to match(//)
  end
end
