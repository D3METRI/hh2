require 'rails_helper'

RSpec.describe "bonus/index", type: :view do
  before(:each) do
    assign(:bonus, [
      Bonu.create!(
        :amount => "9.99",
        :reason => "Reason",
        :user => nil
      ),
      Bonu.create!(
        :amount => "9.99",
        :reason => "Reason",
        :user => nil
      )
    ])
  end

  it "renders a list of bonus" do
    render
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => "Reason".to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
