require 'rails_helper'

RSpec.describe "rowts/show", type: :view do
  before(:each) do
    @rowt = assign(:rowt, Rowt.create!(
      :name => "Name"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
  end
end
