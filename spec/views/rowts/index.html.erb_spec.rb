require 'rails_helper'

RSpec.describe "rowts/index", type: :view do
  before(:each) do
    assign(:rowts, [
      Rowt.create!(
        :name => "Name"
      ),
      Rowt.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of rowts" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
