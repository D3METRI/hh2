require 'rails_helper'

RSpec.describe "rowts/edit", type: :view do
  before(:each) do
    @rowt = assign(:rowt, Rowt.create!(
      :name => "MyString"
    ))
  end

  it "renders the edit rowt form" do
    render

    assert_select "form[action=?][method=?]", rowt_path(@rowt), "post" do

      assert_select "input#rowt_name[name=?]", "rowt[name]"
    end
  end
end
