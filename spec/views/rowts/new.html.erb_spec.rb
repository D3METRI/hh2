require 'rails_helper'

RSpec.describe "rowts/new", type: :view do
  before(:each) do
    assign(:rowt, Rowt.new(
      :name => "MyString"
    ))
  end

  it "renders new rowt form" do
    render

    assert_select "form[action=?][method=?]", rowts_path, "post" do

      assert_select "input#rowt_name[name=?]", "rowt[name]"
    end
  end
end
