class SubdomainPresent
def self.matches?(request)
request.subdomain.present?
end
end

class SubdomainBlank	
def self.matches?(request)
request.subdomain.blank?
end
end

Timetracker::Application.routes.draw do

  constraints(SubdomainPresent) do
match '/', to: 'accounts#new', constraints: { subdomain: 'www' }, via: [:get, :post, :put, :patch, :delete]
	
  resources :bonus do
		get "status", on: :member
		post "status_update", on: :member
	end
  resources :slocs
  get 'work/index'

  get 'work/show'

  get 'users/index'

  get 'users/show'

  get 'users/edit'

  get 'users/update'

  get 'expenses/all' => 'expenses#all', :as => :all_expenses
  get 'rens/all' => 'rens#all', :as => :all_rens
  get 'stats/all' => 'stats#all', :as => :all_stats
  resources :locations
  resources :lineitems
  resources :schedules
  resources :invoices do
    resource :download, only: [:show]
  end
  resources :stats do
		get "status", on: :member
		post "status_update", on: :member
	end
  resources :expenses
	resources :expenses
	resources :stats
	resources :dashboards, only: [:show, :index]
	resources :projects
	resources :works do
	    member do
	    get 'rens/status' # Options: get, patch, put, post, or delete
	    put 'rens/status'
  	    get 'stats/status' # Options: get, patch, put, post, or delete
	    put 'stats/status'
  	    get 'bonus/status' # Options: get, patch, put, post, or delete
	    put 'bonus/status'
  end
	end
    resources :invoices
	resources :rens do
		get "status", on: :member
		post "status_update", on: :member
	end

	resources :messages, only: [:new, :create]
	resources :conversations, only: [:index, :show, :destroy] do
	  member do
	    post :reply, :restore
 	 end	
	  collection do
	    delete :empty_trash
	  end
	end
	resources :steps
	resources :rowts do
		get "rens", on: :member
	end
	devise_for :users, :controllers => { registrations: 'registrations' }
	resources :users do
		get "bonus", on: :member
		post "bonus_update", on: :member
	  get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'    
	  put 'users' => 'devise/registrations#update', :as => 'user_registration'    
	end

        

	resources :workers
	resources :worktypes
	resources :accounts, only: [:show]
	root 'dashboards#index', as: :subdomain_root
end

constraints(SubdomainBlank) do
resources :accounts, only: [:new, :create]
root 'welcomes#index'
end
end