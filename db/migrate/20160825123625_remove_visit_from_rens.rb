class RemoveVisitFromRens < ActiveRecord::Migration
  def change
    remove_column :rens, :visit, :boolean
  end
end
