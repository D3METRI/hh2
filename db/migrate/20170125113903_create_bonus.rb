class CreateBonus < ActiveRecord::Migration
  def change
    create_table :bonus do |t|
      t.decimal :amount
      t.string :reason
      t.date :dayt
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
