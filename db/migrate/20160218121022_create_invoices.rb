class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :name
      t.decimal :amount
      t.date :dayt
      t.references :user, index: true, foreign_key: true
      t.references :ren, index: true, foreign_key: true
      t.references :stat, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
