class RemoveStepIdFromRens < ActiveRecord::Migration
  def change
    remove_column :rens, :step_id, :integer
  end
end
