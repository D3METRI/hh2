class AddBonuIdBillstartBillendBilltoToInvoices < ActiveRecord::Migration
  def change
    add_reference :invoices, :bonu, index: true, foreign_key: true
    add_column :invoices, :billstart, :date
    add_column :invoices, :billend, :date
    add_column :invoices, :billto, :string
  end
end
