class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.datetime :start_time
      t.references :user, index: true, foreign_key: true
      t.references :rowt, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
