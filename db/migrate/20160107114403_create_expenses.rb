class CreateExpenses < ActiveRecord::Migration
  def change
    create_table :expenses do |t|
      t.string :name
      t.decimal :amount
      t.date :dayt
      t.references :user

      t.timestamps null: false
    end
  end
end
