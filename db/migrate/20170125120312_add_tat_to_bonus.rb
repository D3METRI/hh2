class AddTatToBonus < ActiveRecord::Migration
  def change
    add_column :bonus, :tat, :integer, default: 0
  end
end
