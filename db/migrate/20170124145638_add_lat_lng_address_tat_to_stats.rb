class AddLatLngAddressTatToStats < ActiveRecord::Migration
  def change
    add_column :stats, :lat, :float
    add_column :stats, :lng, :float
    add_column :stats, :address, :text
    add_column :stats, :tat, :integer, default: 0
  end
end
