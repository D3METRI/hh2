class CreateStats < ActiveRecord::Migration
  def change
    create_table :stats do |t|
      t.string :name
      t.decimal :amount
      t.date :dayt
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
