class CreateSlocs < ActiveRecord::Migration
  def change
    create_table :slocs do |t|
      t.string :officename
      t.string :address
      t.float :lat
      t.float :lng

      t.timestamps null: false
    end
  end
end
