class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.string :name
      t.text :address
      t.decimal :value, :precision => 9, :scale => 2
      t.integer :rowt_id

      t.timestamps null: false
    end
  end
end
