class AddSteplevelToSteps < ActiveRecord::Migration
  def change
    add_column :steps, :steplevel, :integer, default: 0
  end
end
