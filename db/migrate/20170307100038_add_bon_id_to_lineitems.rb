class AddBonIdToLineitems < ActiveRecord::Migration
  def change
    add_reference :lineitems, :bonu, index: true, foreign_key: true
  end
end
