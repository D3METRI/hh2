class AddFlatrateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :flatrate, :decimal, :precision => 9, :scale => 2, :default => 0.00
    add_column :users, :paylevel, :integer, default: 0
  end
end
