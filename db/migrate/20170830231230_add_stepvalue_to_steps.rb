class AddStepvalueToSteps < ActiveRecord::Migration
  def change
    add_column :steps, :stepvalue, :decimal, :precision => 9, :scale => 2, :default => 0.00
  end
end
