class AddPercentToUsers < ActiveRecord::Migration
  def change
    add_column :users, :percent, :decimal, :precision => 3, :default => 0
  end
end
