class RemoveValueFromSteps < ActiveRecord::Migration
  def change
    remove_column :steps, :value, :decimal
  end
end
