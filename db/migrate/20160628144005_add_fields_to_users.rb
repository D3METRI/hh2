	class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :userlevel, :integer, default: 1
    add_column :users, :firstname, :string
    add_column :users, :lastname, :string
    add_column :users, :phonenumber, :integer, :precision => 10
    add_column :users, :premiumstop, :decimal, :precision => 9, :scale => 2
    add_column :users, :regularstop, :decimal, :precision => 9, :scale => 2
  end
end
