class AddVisitToRens < ActiveRecord::Migration
  def change
    add_column :rens, :visit, :text, array: true, default: []
  end
end
