class AddPvisitToRens < ActiveRecord::Migration
  def change
    add_column :rens, :pvisit, :text, array: true, default: []
  end
end
