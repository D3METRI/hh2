class AddSlocIdToSchedules < ActiveRecord::Migration
  def change
    add_reference :schedules, :sloc, index: true, foreign_key: true
  end
end
