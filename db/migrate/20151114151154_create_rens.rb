class CreateRens < ActiveRecord::Migration
  def change
    create_table :rens do |t|
      t.references :step, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.boolean :visit
      t.timestamps :rentim

      t.timestamps null: false
    end
  end
end
