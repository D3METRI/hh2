class AddSlocIdToRowts < ActiveRecord::Migration
  def change
    add_reference :rowts, :sloc, index: true, foreign_key: true
  end
end
