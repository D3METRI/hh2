class AddLatLngToSteps < ActiveRecord::Migration
  def change
    add_column :steps, :lat, :float
    add_column :steps, :lng, :float
  end
end
