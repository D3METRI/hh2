class CreateRowts < ActiveRecord::Migration
  def change
    create_table :rowts do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
