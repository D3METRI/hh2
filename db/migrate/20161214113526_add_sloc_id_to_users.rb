class AddSlocIdToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :sloc, index: true, foreign_key: true
  end
end
