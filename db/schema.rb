# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171116224108) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "accounts", force: :cascade do |t|
    t.string   "subdomain"
    t.integer  "owner_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "bonus", force: :cascade do |t|
    t.decimal  "amount"
    t.string   "reason"
    t.date     "dayt"
    t.integer  "user_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "tat",        default: 0
  end

  add_index "bonus", ["user_id"], name: "index_bonus_on_user_id", using: :btree

  create_table "expenses", force: :cascade do |t|
    t.string   "name"
    t.decimal  "amount"
    t.date     "dayt"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "category"
  end

  create_table "invoices", force: :cascade do |t|
    t.string   "name"
    t.date     "dayt"
    t.integer  "user_id"
    t.integer  "ren_id"
    t.integer  "stat_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "bonu_id"
    t.date     "billstart"
    t.date     "billend"
    t.string   "billto"
  end

  add_index "invoices", ["bonu_id"], name: "index_invoices_on_bonu_id", using: :btree
  add_index "invoices", ["ren_id"], name: "index_invoices_on_ren_id", using: :btree
  add_index "invoices", ["stat_id"], name: "index_invoices_on_stat_id", using: :btree
  add_index "invoices", ["user_id"], name: "index_invoices_on_user_id", using: :btree

  create_table "lineitems", force: :cascade do |t|
    t.integer  "invoice_id"
    t.integer  "stat_id"
    t.integer  "ren_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "bonu_id"
  end

  add_index "lineitems", ["bonu_id"], name: "index_lineitems_on_bonu_id", using: :btree
  add_index "lineitems", ["invoice_id"], name: "index_lineitems_on_invoice_id", using: :btree
  add_index "lineitems", ["ren_id"], name: "index_lineitems_on_ren_id", using: :btree
  add_index "lineitems", ["stat_id"], name: "index_lineitems_on_stat_id", using: :btree

  create_table "locations", force: :cascade do |t|
    t.string   "address"
    t.float    "lat"
    t.float    "lng"
    t.integer  "step_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "locations", ["step_id"], name: "index_locations_on_step_id", using: :btree

  create_table "mailboxer_conversation_opt_outs", force: :cascade do |t|
    t.integer "unsubscriber_id"
    t.string  "unsubscriber_type"
    t.integer "conversation_id"
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree

  create_table "mailboxer_conversations", force: :cascade do |t|
    t.string   "subject",    default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "mailboxer_notifications", force: :cascade do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              default: ""
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "conversation_id"
    t.boolean  "draft",                default: false
    t.string   "notification_code"
    t.integer  "notified_object_id"
    t.string   "notified_object_type"
    t.string   "attachment"
    t.datetime "updated_at",                           null: false
    t.datetime "created_at",                           null: false
    t.boolean  "global",               default: false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type", using: :btree

  create_table "mailboxer_receipts", force: :cascade do |t|
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.integer  "notification_id",                            null: false
    t.boolean  "is_read",                    default: false
    t.boolean  "trashed",                    default: false
    t.boolean  "deleted",                    default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.boolean  "is_delivered",               default: false
    t.string   "delivery_method"
    t.string   "message_id"
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree

  create_table "rens", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.datetime "dayt"
    t.text     "rvisit",     default: [],              array: true
    t.integer  "rowt_id"
    t.integer  "tat",        default: 0
    t.text     "pvisit",     default: [],              array: true
  end

  add_index "rens", ["user_id"], name: "index_rens_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "rowts", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "sloc_id"
  end

  add_index "rowts", ["sloc_id"], name: "index_rowts_on_sloc_id", using: :btree

  create_table "schedules", force: :cascade do |t|
    t.datetime "start_time"
    t.integer  "user_id"
    t.integer  "rowt_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "sloc_id"
  end

  add_index "schedules", ["rowt_id"], name: "index_schedules_on_rowt_id", using: :btree
  add_index "schedules", ["sloc_id"], name: "index_schedules_on_sloc_id", using: :btree
  add_index "schedules", ["user_id"], name: "index_schedules_on_user_id", using: :btree

  create_table "slocs", force: :cascade do |t|
    t.string   "officename"
    t.string   "address"
    t.float    "lat"
    t.float    "lng"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stats", force: :cascade do |t|
    t.string   "name"
    t.decimal  "amount"
    t.date     "dayt"
    t.integer  "user_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.float    "lat"
    t.float    "lng"
    t.text     "address"
    t.integer  "tat",        default: 0
  end

  add_index "stats", ["user_id"], name: "index_stats_on_user_id", using: :btree

  create_table "steps", force: :cascade do |t|
    t.string   "name"
    t.text     "address"
    t.integer  "rowt_id"
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
    t.integer  "steplevel",                          default: 0
    t.float    "lat"
    t.float    "lng"
    t.decimal  "stepvalue",  precision: 9, scale: 2, default: 0.0
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email",                                          default: "",  null: false
    t.string   "encrypted_password",                             default: "",  null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",                              default: 0
    t.integer  "userlevel",                                      default: 1
    t.string   "firstname"
    t.string   "lastname"
    t.integer  "phonenumber"
    t.decimal  "premiumstop",            precision: 9, scale: 2
    t.decimal  "regularstop",            precision: 9, scale: 2
    t.string   "picture"
    t.integer  "sloc_id"
    t.decimal  "flatrate",               precision: 9, scale: 2, default: 0.0
    t.integer  "paylevel",                                       default: 0
    t.decimal  "percent",                precision: 3,           default: 0
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["sloc_id"], name: "index_users_on_sloc_id", using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  add_foreign_key "bonus", "users"
  add_foreign_key "invoices", "bonus"
  add_foreign_key "invoices", "rens"
  add_foreign_key "invoices", "stats"
  add_foreign_key "invoices", "users"
  add_foreign_key "lineitems", "bonus"
  add_foreign_key "lineitems", "invoices"
  add_foreign_key "lineitems", "rens"
  add_foreign_key "lineitems", "stats"
  add_foreign_key "locations", "steps"
  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", column: "conversation_id", name: "mb_opt_outs_on_conversations_id"
  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", column: "conversation_id", name: "notifications_on_conversation_id"
  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", column: "notification_id", name: "receipts_on_notification_id"
  add_foreign_key "rens", "users"
  add_foreign_key "rowts", "slocs"
  add_foreign_key "schedules", "rowts"
  add_foreign_key "schedules", "slocs"
  add_foreign_key "schedules", "users"
  add_foreign_key "stats", "users"
  add_foreign_key "users", "slocs"
end
