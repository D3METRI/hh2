// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require_self
//= require bootstrap/collapse
//= require bootstrap/modal
//= require bootstrap/button
//= require bootstrap/affix
//= require bootstrap/tab
//= require bootstrap/alert
//= require bootstrap/transition
//= require bootstrap/tooltip
//= require bootstrap/popover
//= require bootstrap/scrollspy
//= require bootstrap/dropdown
//= require bootstrap/carousel
//= require turbolinks
//= require cocoon
//= require chosen-jquery
//= require highcharts.js
//= require chartkick
//= require rails.validations
//= require rails.validations.simple_form
//= require highcharts/highcharts
//= require highcharts/highcharts-more
//= require highcharts/highstock
