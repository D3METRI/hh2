class Stat < ActiveRecord::Base
  include PublicActivity::Model
  tracked owner: Proc.new{ |controller, model| controller.current_user }	
      enum tat: [:submitted, :approved, :denied]





    geocoded_by :address, :latitude  => :lat, :longitude => :lng
    after_validation :geocode
  scope :pending, -> { where(:tat => ["0", "2"]) }
		validates :name, presence: true
		validates :amount, presence: true
		validates :dayt, presence: true
  belongs_to :user
  has_one :lineitem
  has_one :invoice, through: :lineitems
  scope :created_between, lambda {|start_date, end_date| where("created_at >= ? AND created_at <= ?", start_date, end_date )}
  scope :created_at, ->(month) { where( "Month( created_at ) = ?", month ) }
  	  scope :this_month, -> { where(created_at: Time.now.beginning_of_month..Time.now.end_of_month) }
        scope :pending, -> { where(:tat => ["0", "2"]) }
        scope :rejected, -> { where(:tat => ["2"]) }
end
