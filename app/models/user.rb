class User < ActiveRecord::Base

   mount_uploader :picture, PictureUploader
  enum userlevel: [:inactive, :driver, :dispatch, :admin]
    enum paylevel: [:perstop, :perc, :flat]
rolify :role_cname => 'MyEngine::Role'

 belongs_to :sloc

  acts_as_messageable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :recoverable, :rememberable, :validatable

  validates :name, presence: true

  has_many :invoices, through: :lineitems, :dependent => :destroy
 accepts_nested_attributes_for :invoices
  has_many :rens, :dependent => :destroy
  has_many :expenses, :dependent => :destroy
  has_many :stats, :dependent => :destroy
  has_many :steps, through: :rens, :dependent => :destroy
  accepts_nested_attributes_for :rens
  has_many :bonu, :dependent => :destroy
  accepts_nested_attributes_for :bonu

  scope :dispatch, -> { where(:userlevel => ["dispatch"]) }
  scope :officeusers, -> { where(:sloc => ["driver"]) }

  def self.officeu
    User.joins(:slocs).where("sloc.name < ?", Time.now)
  end

def mailboxer_email(object)
  email
end


  # required for avatarable
  def avatar_text
    firstname.chr
  end

   def percent_of(n)
    self.to_f / n.to_f * 100.0
   end
  

  def update_password_with_password(params, *options)
    current_password = params.delete(:current_password)

    result = if valid_password?(current_password)
               update_attributes(params, *options)
             else
               self.assign_attributes(params, *options)
               self.valid?
               self.errors.add(:current_password, current_password.blank? ? :blank : :invalid)
               false
             end

    clean_up_passwords
    result
  end

end
