class Schedule < ActiveRecord::Base
  belongs_to :user
  belongs_to :rowt
  belongs_to :sloc

  scope :this_month, -> { where(start_time: Time.now.beginning_of_month..Time.now.end_of_month) }
   scope :this_week, -> { where(start_time: Time.now-3.days..Time.now+7.days) }
end
