class Rowt < ActiveRecord::Base
	has_many :steps, :dependent => :destroy
	has_many :rens
	belongs_to :sloc
		validates :name, presence: true
		validates :steps, presence: true
	
	accepts_nested_attributes_for :rens, allow_destroy: true
	accepts_nested_attributes_for :steps, allow_destroy: true
end
