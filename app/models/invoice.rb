require "render_anywhere"

class Invoice < ActiveRecord::Base
  belongs_to :user
  has_many :rens, through: :lineitems, :dependent => :delete_all
  has_many :stats, through: :lineitems, :dependent => :delete_all
  has_many :bonus, through: :lineitems, :dependent => :delete_all
  has_many :lineitems, dependent: :destroy
  accepts_nested_attributes_for :lineitems, :stats, :rens

  include RenderAnywhere
 

 
  def to_pdf
    kit = PDFKit.new(as_html, page_size: 'A4')
    kit.to_file("#{Rails.root}/public/invoice.pdf")
  end
 
  def filename
    "Invoice #{invoice.id}.pdf"
  end
 
  private
 
    attr_reader :invoice
 
    def as_html
      render template: "invoices/pdf", layout: "invoice_pdf", locals: { invoice: invoice }
    end



end
