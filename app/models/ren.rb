class Ren < ActiveRecord::Base
  include PublicActivity::Model
  tracked owner: Proc.new{ |controller, model| controller.current_user }
    enum tat: [:submitted, :approved, :denied]

  belongs_to :user
  belongs_to :rowt
  has_one :lineitem
  has_one :invoice, through: :lineitem
  has_one :lineitem
  has_one :sloc, through: :user
    scope :this_month, -> { where(created_at: Time.now.beginning_of_month..Time.now.end_of_month) }
	scope :pending, -> { where(:tat => ["0", "2"]) }
        scope :rejected, -> { where(:tat => ["2"]) }
	scope :dispatch, -> { where(:userlevel => ["dispatch"]) }
	scope :dispatchrens, -> { dispatch.pending }

def week
  self.dayt.strftime('%W')
end

end
