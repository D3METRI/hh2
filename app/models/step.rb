class Step < ActiveRecord::Base
	belongs_to :rowt
    enum steplevel: [:regular, :premium]

	has_many :users, through: :rens, :source => :user
	has_one :location
	validates :name, presence: true
	validates :address, presence: true


	geocoded_by :address, :latitude  => :lat, :longitude => :lng
    after_validation :geocode

	accepts_nested_attributes_for :location
end
