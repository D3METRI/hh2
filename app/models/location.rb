class Location < ActiveRecord::Base
  belongs_to :steps
  geocoded_by :address, :latitude  => :lat, :longitude => :lng
  after_validation :geocode
end
