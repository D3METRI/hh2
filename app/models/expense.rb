class Expense < ActiveRecord::Base
	belongs_to :user
	validates :category, presence: true
	validates :amount, presence: true
	validates :name, presence: true
	validates :dayt, presence: true
	  scope :this_month, -> { where(created_at: Time.now.beginning_of_month..Time.now.end_of_month) }
end
