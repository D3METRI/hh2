class Bonu < ActiveRecord::Base
  belongs_to :user
  has_one :lineitem
  has_one :invoice, through: :lineitems
       enum tat: [:submitted, :approved, :denied]

       scope :pending, -> { where(:tat => ["0", "2"]) }
        scope :rejected, -> { where(:tat => ["2"]) }
end
