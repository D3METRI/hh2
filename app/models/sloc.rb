class Sloc < ActiveRecord::Base
		has_many :users
		has_many :rowts
		has_many :schedules
		has_many :rens, through: :user
		validates :officename, presence: true
		validates :address, presence: true
	  geocoded_by :address, :latitude  => :lat, :longitude => :lng
      after_validation :geocode
end
