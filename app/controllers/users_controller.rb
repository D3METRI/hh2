class UsersController < ApplicationController


  before_action :authenticate_user!


  def index
  	@users = User.all
	@user = current_user
  end

  def show
    @user = User.find(params[:id])
    redirect_to users_path if @user.id != current_user.id unless current_user.admin? unless current_user.dispatch?
  end

  def edit
  		@user = User.find(params[:id])
      @offices = Sloc.all
      @paylevel = ["perstop", "perc", "flat"]
    redirect_to users_path if @user.id != current_user.id unless current_user.admin? unless current_user.dispatch?
    @val = 100
    @val1 = 10
  end

  def update
  	@user = User.find(params[:id])
    respond_to do |format|
      if @user.update(user_params)
        flash[:notice] = "Your account has been updated"
        format.json { render :json => @user.to_json, :status => 200 }
        format.xml  { head :ok }
        format.html { render :action => :show }
      else
        format.json { render :text => "Could not update user", :status => :unprocessable_entity } #placeholder
        format.xml  { render :xml => @user.errors, :status => :unprocessable_entity }
        format.html { render :action => :edit, :status => :unprocessable_entity }
      end
	  end
  end

def bonus
  @user = User.find(params[:id]) 
  @bonu = Bonu.new 
end

def bonus_update
    @user = User.find(params[:id])
    @bonu = Bonu.new(bonu_params)

    respond_to do |format|
      if @bonu.save
        format.html { redirect_to @bonu, notice: 'Bonus was successfully added.' }
        format.json { render :show, status: :created, location: @bonu }
      else
        format.html { render :new }
        format.json { render json: @bonu.errors, status: :unprocessable_entity }
      end
    end

end



  def update_password
    @user = User.find(current_user.id)
    if @user.update(user_params)
      # Sign in the user by passing validation in case their password changed
      bypass_sign_in(@user)
      redirect_to root_path
    else
      render "edit"
    end
  end

private

	def user_params
	  params[:user].permit(:name, :firstname, :lastname, :phonenumber, :regularstop, :premiumstop, :userlevel, :email, :flatrate, :paylevel, :picture, :sloc_id, :bonu_id, :password, :password_confirmation, :current_password, :percent)
   end
end
