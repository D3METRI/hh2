class SchedulesController < ApplicationController
  before_action :set_schedule, only: [:show, :edit, :update, :destroy]

  # GET /schedules
  # GET /schedules.json
  def index
    @schedules = Schedule.this_month.order('start_time DESC')
    @rowts = Rowt.all
    @user = current_user
    @schedule = Schedule.where(:user_id => current_user.id)
  end

  # GET /schedules/1
  # GET /schedules/1.json
  def show
    @user = current_user
    @schedule = Schedule.find(params[:id])
    redirect_to schedules_path if @schedule.user.id != current_user.id unless current_user.admin? unless current_user.dispatch?
  end

  # GET /schedules/new
  def new
    @user = current_user
    @office = @user.sloc
    @schedule = Schedule.new
    @office2 = Sloc.all
    @users = User.joins(:sloc).where(:sloc => current_user.sloc)
    @rowts = Rowt.joins(:sloc).where(:sloc => @office)
    redirect_to schedules_path if current_user.driver?
  end

  # GET /schedules/1/edit
  def edit
    @schedule = Schedule.find(params[:id])
    @office = @schedule.sloc    
    redirect_to schedules_path if current_user.driver?
  end

  # POST /schedules
  # POST /schedules.json
  def create
    @schedule = Schedule.new(schedule_params)

    respond_to do |format|
      if @schedule.save
        format.html { redirect_to schedules_path, notice: 'Schedule was successfully created.' }
        format.json { render :index, status: :created, location: schedules_path }
      else
        format.html { render :new }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schedules/1
  # PATCH/PUT /schedules/1.json
  def update
    respond_to do |format|
      if @schedule.update(schedule_params)
        format.html { redirect_to @schedule, notice: 'Schedule was successfully updated.' }
        format.json { render :show, status: :ok, location: @schedule }
      else
        format.html { render :edit }
        format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schedules/1
  # DELETE /schedules/1.json
  def destroy
    @schedule = Schedule.find(params[:id])
    authorize @schedule
    @schedule.destroy
    respond_to do |format|
      format.html { redirect_to schedules_url, notice: 'Schedule was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schedule
      @schedule = Schedule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schedule_params
      params.require(:schedule).permit(:start_time, :user_id, :rowt_id, :sloc_id)
    end
end
