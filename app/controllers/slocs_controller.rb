class SlocsController < ApplicationController
  before_action :set_sloc, only: [:show, :edit, :update, :destroy]

  # GET /slocs
  # GET /slocs.json
  def index
    @slocs = Sloc.all
  end

  # GET /slocs/1
  # GET /slocs/1.json
  def show
  end

  # GET /slocs/new
  def new
    @sloc = Sloc.new
  end

  # GET /slocs/1/edit
  def edit
  end

  # POST /slocs
  # POST /slocs.json
  def create
    @sloc = Sloc.new(sloc_params)

    respond_to do |format|
      if @sloc.save
        format.html { redirect_to @sloc, notice: 'Location was successfully created.' }
        format.json { render :show, status: :created, location: @sloc }
      else
        format.html { render :new }
        format.json { render json: @sloc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /slocs/1
  # PATCH/PUT /slocs/1.json
  def update
    respond_to do |format|
      if @sloc.update(sloc_params)
        format.html { redirect_to @sloc, notice: 'Location was successfully updated.' }
        format.json { render :show, status: :ok, location: @sloc }
      else
        format.html { render :edit }
        format.json { render json: @sloc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /slocs/1
  # DELETE /slocs/1.json
  def destroy
    @sloc.destroy
    respond_to do |format|
      format.html { redirect_to slocs_url, notice: 'Location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def au
    @users = User.where(sloc: nil)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sloc
      @sloc = Sloc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sloc_params
      params.require(:sloc).permit(:officename, :address, :lat, :lng)
    end
end
