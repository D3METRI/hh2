class RegistrationsController < Devise::RegistrationsController

	skip_before_filter :check_payment2

  private

  def sign_up_params
    params.require(:user).permit(:name, :firstname, :lastname, :email, :phonenumber, :premiumstop, :regularstop, :userlevel, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:name, :firstname, :lastname, :email, :phonenumber, :premiumstop, :regularstop, :userlevel, :password, :password_confirmation, :current_password)
  end
end