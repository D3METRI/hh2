class RensController < ApplicationController
  before_action :set_ren, only: [:show, :edit, :update, :destroy]

skip_before_action :verify_authenticity_token


  # GET /rens
  # GET /rens.json
  def index
    @user = current_user
    @rens = @user.rens.this_month.all
    @allrens = Ren.all
    @paylevel = @user.paylevel

  end

  def all
    @user = current_user
    @rens = @user.rens.all
    @search = @rens.search(params[:q])
    @rensresults = @search.result
    @paylevel = @user.paylevel
  end



  # GET /rens/1
  # GET /rens/1.json
  def show
    @paylevel = @ren.user.paylevel
    @ren = Ren.find(params[:id])
    @user = current_user
    @flatrate = @user.flatrate
    @pvalue = @user.premiumstop
    @rvalue = @user.regularstop
    @visited = (@ren.rvisit + @ren.pvisit) #array of ids for visited
    @visitedstep = Step.where(id: [@visited])
    @regularvisit = @visitedstep.where(steplevel: 0)
    @premiumvisit = @visitedstep.where(steplevel: 1)
    @total = (@premiumvisit.count * @pvalue) + (@regularvisit.count * @rvalue)




    @pstepid = @ren.pvisit
    @pstep = Step.where(id: [@pstepid])
    @stepid = @ren.rvisit
    @rstep = Step.where(id: [@stepid])
    @ss = @pstep.concat @rstep
    @stepvaluetotal = @ss.inject(0){|sum,e| sum + e.stepvalue }
    @pertotal = (@stepvaluetotal * (@user.percent * 0.01))

    redirect_to rens_path if @ren.user.id != current_user.id unless current_user.admin? unless current_user.dispatch?
  end

  # GET /rens/new
  def new
    ###ren is set through rowt/ren
    @k = "1"
    @user = current_user
    @steps = Step.all
    @ren = Ren.new
    @rens = @user.rens.all
    @rowt = Rowt.first
    @r = Step.where(:rowt_id => @k)
  end

  # GET /rens/1/edit
  def edit
    @ren = Ren.find(params[:id])
    @rowtt = @ren.rowt
    @steps = Step.all
    @s = Step.where(:rowt_id => @rowtt)
    @user = current_user
    @k = "1"
    @r = Step.where(:rowt_id => @k)
    @visit = Ren.find_by(params[:visit])

   @pstepid = @ren.pvisit
    @pstep = Step.where(id: [@pstepid])

    @stepid = @ren.rvisit
    @rstep = Step.where(id: [@stepid])
    redirect_to rens_path if @ren.user.id != current_user.id || @ren.tat = "approved"
  end

  # POST /rens
  # POST /rens.json
  def create
      @ren = Ren.new(ren_params)

    respond_to do |format|
      if @ren.save
        format.html { redirect_to @ren, notice: 'Run added successfully!' }
        format.json { render :show, status: :created, location: @ren }
      else
        format.html { render :new }
        format.json { render json: @ren.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rens/1
  # PATCH/PUT /rens/1.json
  def update
    respond_to do |format|
      if @ren.update(ren_params)
          authorize @ren
        format.html { redirect_to @ren, notice: 'Ren was successfully updated.' }
        format.json { render :show, status: :ok, location: @ren }
      else
        format.html { render :edit }
        format.json { render json: @ren.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rens/1
  # DELETE /rens/1.json
  def destroy
    @ren.destroy
    respond_to do |format|
      format.html { redirect_to rens_url, notice: 'Ren was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def status
    @ren = Ren.find(params[:id])
    @rowtt = @ren.rowt
    @stepid = @ren.rvisit + @ren.pvisit
    @step = Step.where(id: [@stepid])
    @driver = @ren.user

    @pvalue = @driver.premiumstop
    @rvalue = @driver.regularstop
    @pvisit = @ren.pvisit.reject!(&:empty?).count
    @rvisit = @ren.rvisit.reject!(&:empty?).count

    @ptotal = @pvalue * @pvisit
    @rtotal = @rvalue * @rvisit
    @total = (@ptotal + @rtotal)
  redirect_to rens_path if current_user.driver?
  end

  def status_update
    @ren = Ren.find params[:id]
    respond_to do |format|
      if @ren.update(ren_params)
        format.html { redirect_to works_path, notice: 'Work status was successfully updated.' }
        format.json { redirect_to controller: :works, action: :index, status: :ok, location: :works }
      else
        format.html { render :status }
        format.json { render json: @ren.errors, status: :unprocessable_entity }
      end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ren
      @ren = Ren.find(params[:id])
    end

          def rens_params(my_params)
            my_params.permit(:ren_id, :step_id, :user_id, :visit, :dayt)
          end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ren_params
      params.require(:ren).permit(:dayt, :user_id, :rowt_id, :tat, {:rvisit => []}, {:pvisit => []})
    end
end
