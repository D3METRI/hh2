class StatsController < ApplicationController
  before_action :set_stat, only: [:show, :edit, :update, :destroy]
  before_action :load_activities, only: [:index, :show, :new, :edit]

  # GET /stats
  # GET /stats.json
  def index
    @user = current_user
    @stats = @user.stats.this_month.all
    @month = Date.today.strftime("%m")
      @activities = PublicActivity::Activity.all
  end

  def all
    @user = current_user
    @stats = @user.stats.all
    @search = @stats.search(params[:q])
    @statsresults = @search.result
  end


  # GET /stats/1
  # GET /stats/1.json
  def show
    @user = current_user
    @office = @user.sloc
    @address1 = @office.address
    @stat = Stat.find(params[:id])
    @address2 = @stat.address
    redirect_to stats_path if @stat.user.id != current_user.id unless current_user.admin? unless current_user.dispatch?
  end

  # GET /stats/new
  def new
    @stat = Stat.new

  end

  # GET /stats/1/edit
  def edit
    @user = current_user
    @stat = Stat.find(params[:id])
    redirect_to stats_path if @stat.user.id != current_user.id
  end

  # POST /stats
  # POST /stats.json
  def create
    @stat = Stat.new(stat_params)
    respond_to do |format|
      if @stat.save
        format.html { redirect_to stats_path, notice: 'Stat was successfully created.' }
        format.json { render :show, status: :created, location: stats_path }
      else
        format.html { render :new }
        format.json { render json: @stat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stats/1
  # PATCH/PUT /stats/1.json
  def update
    respond_to do |format|
      if @stat.update(stat_params)
        format.html { redirect_to @stat, notice: 'Stat was successfully updated.' }
        format.json { render :show, status: :ok, location: @stat }
      else
        format.html { render :edit }
        format.json { render json: @stat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stats/1
  # DELETE /stats/1.json
  def destroy
    @stat.destroy
    respond_to do |format|
      format.html { redirect_to stats_url, notice: 'Stat was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def status
  @stat = Stat.find(params[:id])    
  redirect_to stats_path if current_user.driver?
  end

  def status_update
    @stat = Stat.find params[:id]
    respond_to do |format|
      if @stat.update(stat_params)
        format.html { redirect_to works_path, notice: 'Work status was successfully updated.' }
        format.json { redirect_to controller: :works, action: :index, status: :ok, location: :works }
      else
        format.html { render :status }
        format.json { render json: @stat.errors, status: :unprocessable_entity }
      end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stat
      @stat = Stat.find(params[:id])
    end

    def load_activities
      @activities = PublicActivity::Activity.order('created_at DESC').limit(20)
    end


    # Never trust parameters from the scary internet, only allow the white list through.
    def stat_params
      params.require(:stat).permit(:name, :amount, :dayt, :user_id, :address, :lat, :lng, :tat)
    end
end
