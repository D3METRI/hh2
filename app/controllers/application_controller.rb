class ApplicationController < ActionController::Base
  include Pundit
  include PublicActivity::StoreController
  include ApplicationHelper
  helper_method :current_dispatch
  helper_method :current_admin
  helper_method :user_not_authorized
protect_from_forgery with: :exception

before_filter :load_schema, :authenticate_user!
before_filter :configure_permitted_parameters, if: :devise_controller?


rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
	
	before_action :authenticate_user!
	before_action :check_payment2, unless: :devise_controller?



	protected

	def configure_permitted_parameters
		devise_parameter_sanitizer.for(:accept_invitation).concat([:name])	
            devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :password) }
            devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :email, :password, :current_password, :password_confirmation) }
	end

	private


		def user_not_authorized
		  flash[:warning] = "You are not authorized to do to this."
		  redirect_to(request.referrer || root_path)
		end

		def load_schema
			Apartment::Tenant.switch!('public')
			return unless request.subdomain.present?
			

    		if current_account
      		Apartment::Tenant.switch!(current_account.subdomain)
    		else
      		redirect_to root_url(subdomain: false)
    		end
  		end

	  def current_account
    	@current_account ||= Account.find_by(subdomain: request.subdomain)
  	  end
	helper_method :current_account

	def set_mailer_host
		subdomain = current_account ? "#{current_account.subdomain}." : ""
		ActionMailer::Base.default_url_options[:host] = "#{subdomain}hhonk.com"
		
	end

	def check_payment2
	  @user = current_user
	  @sloc = @user.sloc
	  unless @user.admin? || @user.dispatch?

	  if User.where(:sloc_id => current_user.sloc_id).present? && @user.regularstop == 0 && @user.premiumstop == 0 && @user.percent == 0 && @user.flatrate == 0
	    redirect_to "/erroruser.html"
	  else
	    # do something else
	  end
	end
	end

	def check_payment
	  @user = current_user
    unless @user.admin?
	  if @user.regularstop == 0
	    redirect_to "/erroruser.html"
	  elsif 
	  	@user.premiumstop == 0 
	    redirect_to "/erroruser.html"
	  elsif @user.flatrate == 0 
	    redirect_to "/erroruser.html"
	  elsif @user.percent == 0
	    redirect_to "/erroruser.html"

	    end
	  end
	end


	def after_sign_out_path_for(resource_or_scope)
		new_user_session_path
	end

	def after_invite_path_for(resource)
		users_path
	end



end