class AccountsController < ApplicationController

	skip_before_filter :authenticate_user!, only: [:new, :create]
		skip_before_filter :check_payment2

	def new
	@account = Account.new
	@account.build_owner
	end


	def create
		@account = Account.new(account_params)
		if @account.valid?
			Apartment::Database.create(@account.subdomain)
			Apartment::Database.switch(@account.subdomain)
			@account.save
			redirect_to new_user_session_url(subdomain: @account.subdomain)
		else
			render action: 'new'
		end
	end

	def show
		@account = current_account
	end

			rescue_from ActiveRecord::RecordNotFound do
			  flash[:warning] = 'Resource not found.'
			  redirect_back_or root_path
			end

			def redirect_back_or(path)
			  redirect_to request.referer || path
			end




	private
		def account_params
		params.require(:account).permit(:subdomain, owner_attributes: [:name, :userlevel, :email, :password, :password_confirmation])
		end
end

