class WorkersController < ApplicationController
	
	def index
		@users = User.all
		@user = current_user
	end

	def show
		@user = current_user
 		@rens = @user.rens.all
 		@rs = current_user.rens.group_by{|ren| [ren.dayt, ren.rowt.name] }
 		@date = Date.today
 		@schedule = Schedule.where(id: @user.id)
	end


end