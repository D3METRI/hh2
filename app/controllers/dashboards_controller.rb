class DashboardsController < ApplicationController

	before_action :load_activities, only: [:index, :show, :new, :edit]
	skip_before_filter :check_payment2
	skip_before_filter :check_sloc

	def index
		@rowts = Rowt.all
		@user = current_user
		@partial = @user.userlevel
		@schedules = Schedule.all
	    @schedule = Schedule.where(:user_id => current_user.id).where(start_time: Time.now-3.days..Time.now+7.days).order('start_time ASC')
	    @schedule2 = @schedule.where(start_time: Time.now-3.days..Time.now+7.days)
	    @driverrens = @user.rens.this_month.all
	    @driverstats = @user.stats.this_month.all
	    @expenses = @user.expenses.this_month.all
	    @income = @user.stats.this_month.all
	    @officeuser = @user.sloc
	    @workers = User.where(:sloc => @officeuser)
		@test = Stat.pending	    

		#ren, stat, or bonu with submitted or denied status
	    @pendingrens = Ren.pending
	    @pendingbonus = Bonu.pending
	    @pendingstats = Stat.pending
	    @pendingwork = (@pendingrens + @pendingstats + @pendingbonus)

	    @rejectedrens = Ren.rejected
	    @rejectedbonus = Bonu.rejected
	    @rejectedstats = Stat.rejected
	    @rejectedwork = (@rejectedrens + @rejectedstats + @rejectedbonus)	    

	    #calendar
	    @schedule2 = Schedule.where("start_time >= ? and sloc_id >= ?", 1.day.from_now, @user.sloc_id).order('start_time ASC')
	    @schedule3 = Schedule.joins(:user).where("start_time >= ?", 1.days.ago).where("start_time <= ?", 1.day.from_now).where("users.sloc_id >= ?", @user.sloc_id).order('start_time ASC')

	    #Admin
	    @aworkers = User.all
	    @slocs = Sloc.all
	    @activities = PublicActivity::Activity.all
	end

	def show
	end

	private

def find_office
	@driversrens
end


def load_activities
  @activities = PublicActivity::Activity.order('created_at DESC').limit(20)
end

	def premiumvisit
		@ren = @driverrens.each
		@ren.visit()		
	end

end
