class RowtsController < ApplicationController
  before_action :set_rowt, only: [:show, :edit, :update, :destroy, :rens]
  before_action :check_payment2

  # GET /rowts
  # GET /rowts.json
  def index
    @user = current_user
    @office = @user.sloc
    @rowts = Rowt.where(sloc: @office)
  end

  # GET /rowts/1
  # GET /rowts/1.json
  def show
    @rowt = set_rowt
    @steps = @rowt.steps
    @address1 = "101 W 5th St, Tempe, AZ 85281"
    @address2 = "90210"
  end

  # GET /rowts/new
  def new
    @rowt = Rowt.new
    @steps = @rowt.steps.build
    @slocs = Sloc.all
    redirect_to rowts_path if current_user.driver? || current_user.dispatch?
  end

  # GET /rowts/1/edit
  def edit
    @rowt = Rowt.find(params[:id])
    @slocs = Sloc.all
    @sloc = @rowt.sloc
    redirect_to rowts_path if current_user.driver? || current_user.dispatch?
  end

  # POST /rowts
  # POST /rowts.json
  def create
    @rowt = Rowt.new(rowt_params)

    respond_to do |format|
      if @rowt.save
        format.html { redirect_to @rowt, notice: 'You created a Route!' }
        format.json { render :show, status: :created, location: @rowt }
      else
        format.html { render :new }
        format.json { render json: @rowt.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rowts/1
  # PATCH/PUT /rowts/1.json
  def update
    respond_to do |format|
      if @rowt.update(rowt_params)
        format.html { redirect_to @rowt, notice: 'Update was successfully!' }
        format.json { render :show, status: :ok, location: @rowt }
      else
        format.html { render :edit }
        format.json { render json: @rowt.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rowts/1
  # DELETE /rowts/1.json
  def destroy
    @rowt = Rowt.find(params[:id])
    authorize @rowt
    @rowt.destroy
    respond_to do |format|
      format.html { redirect_to dashboards_url, notice: 'Route has been removed' }
      format.json { head :no_content }
    end
  end

  def rens
    @k = 1
    @ren = Ren.new
    @users = User.all
    @user = current_user
    @rowtt = Rowt.find(params[:id])
    @s = Step.where(:rowt_id => @rowtt)
    @ps = @s.where(steplevel: "1") # 1 is a premium step
    @rs = @s.where(steplevel: "0") # 1 is a premium step
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rowt
      @rowt = Rowt.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rowt_params
      params.require(:rowt).permit(:name, :sloc_id, steps_attributes: [:name, :steplevel, :address, :pvisit, :rvalue, :id, :_destroy, :stepvalue, :rens_attributes => [:id, :pvisit, :rvisit, :dayt, :user_id, :rowt_id], :location_attributes => [:step_id, :address, :lat, :lng, :id]])
    end
end
