class InvoicesController < ApplicationController
  before_action :set_invoice, only: [:show, :edit, :update, :destroy]

  # GET /invoices
  # GET /invoices.json
  def index
    @user = current_user
    @ui = @user.id
    @invoice = Invoice.where(user_id: @user.id)
    @search = @invoice.search(params[:q])
    @invoices = @search.result

  end

  # GET /invoices/1
  # GET /invoices/1.json
  def show
    @invoice = Invoice.find(params[:id])
    @renvoice = @invoice.rens
    @stattotal = @invoice.stats.map { |h| h[:amount] }.sum
    @arraysteps = @renvoice.each.collect(&:rvisit) ########################was visit, need to add pvisit
    @iuser = User.find_by_id(@invoice.user_id)
    @pstop = @iuser.premiumstop
    @rstop = @iuser.regularstop
    @stepid = @arraysteps.flatten.reject!(&:empty?) #array of just stop ids
    @step = Step.where(id:[@stepid])
    @rvisit = @step.where(steplevel: 0)
    @pvisit = @step.where(steplevel: 1)
    @total1 = []
    @renvoice.each do |r|
      total1 =   (r.rvisit.reject(&:empty?).count * current_user.regularstop) + (r.pvisit.reject(&:empty?).count * current_user.premiumstop)
      @total1.push(total1)
      end
    @rentotal = @total1.inject(0){|sum,x| sum + x }
    @total2 = (@rvisit.count * @rstop)
    @total = (@rentotal + @stattotal )
    #######
    @user = current_user
    @paylevel = @user.paylevel
    @rens = @invoice.rens.all
    @stats = @invoice.stats.all
    @bonu = @invoice.bonus.all

    @flatratetotal = ( @user.flatrate * @renvoice.count)
    @total3 = (@flatratetotal + @stattotal)



    #Percent
     @perctotal = [] 
     @renvoice.each do |b| 
     @pra = b.pvisit.compact.reject(&:blank?) 
    @pra.each do |f| 
       @pb = Step.find_by_id(f)
       @perctotal << @pb.stepvalue 
       end 
       @pra = b.rvisit.compact.reject(&:blank?) 
       @pra.each do |f| 
       @pc = Step.find_by_id(f)
       @perctotal << @pc.stepvalue 
       end 
       @userperc = (@user.percent * 0.01) 
       @zttotal = @perctotal.sum 
      @ftotal = (@perctotal * @userperc )
    end



    @perctotal = @ftotalz

    @bonutotal = @invoice.bonus.map { |h| h[:amount] }.sum
    redirect_to invoices_path if @invoice.user.id != current_user.id 
    respond_to do |format|
      format.html
      format.pdf do
        render pdf: "pdf", :template => 'invoices/pdf.html.erb'
      end
    end   



  end

  # GET /invoices/new
  def new
    @user = current_user
    @paylevel = @user.paylevel
    @invoice = Invoice.new
    @lineitems = @invoice.lineitems.build
    @rens = @user.rens.all
    @rens2 = @user.rens.all
    @stats = @user.stats.all
    @q = Ren.ransack(params[:q])
    @qr = @q.result(distinct: true)
    @pv = @user.premiumstop
    @visited = @rens.where(visit: true)
    @visitedstep = Step.where(id: [@visited])
    @bonu = Bonu.where(user_id: @user.id)



  end

  # GET /invoices/1/edit
  def edit
    @stat = Invoice.find(params[:id])
    @user = current_user
    @invoice = Invoice.find(params[:id])
    @rens = @invoice.rens.all
    @stats = @invoice.stats.all
    @bonu = @invoice.bonus.all
    redirect_to stats_path if @stat.user.id != current_user.id 
  end

  # POST /invoices
  # POST /invoices.json
  def create
    @invoice = Invoice.new(invoice_params)

    respond_to do |format|
      if @invoice.save
        format.html { redirect_to @invoice, notice: 'Invoice was successfully created.' }
        format.json { render :show, status: :created, location: @invoice }
      else
        format.html { render :new }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invoices/1
  # PATCH/PUT /invoices/1.json
  def update
    respond_to do |format|
      if @invoice.update(invoice_params)
        format.html { redirect_to @invoice, notice: 'Invoice was successfully updated.' }
        format.json { render :show, status: :ok, location: @invoice }
      else
        format.html { render :edit }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invoices/1
  # DELETE /invoices/1.json
  def destroy
    @invoice.destroy
    respond_to do |format|
      format.html { redirect_to invoices_url, notice: 'Invoice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice
      @invoice = Invoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invoice_params
      params.require(:invoice).permit(:name, :billto, :billstart, :billend, :dayt, :user_id, :ren_ids => [], :stat_ids => [], :bonu_ids => [])
    end
end
