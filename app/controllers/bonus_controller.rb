class BonusController < ApplicationController
  before_action :set_bonu, only: [:show, :edit, :update, :destroy]

  # GET /bonus
  # GET /bonus.json
  def index
    @user = current_user
    @bonus = @user.bonu.all
  end

  # GET /bonus/1
  # GET /bonus/1.json
  def show
    @bonu = Bonu.find(params[:id])
    redirect_to bonus_path if @bonu.user.id != current_user.id 
  end

  # GET /bonus/new
  def new
    @bonu = Bonu.new
  end

  # GET /bonus/1/edit
  def edit
    @bonu = Bonu.find(params[:id])
    redirect_to bonus_path if @bonu.user.id != current_user.id 
  end

  # POST /bonus
  # POST /bonus.json
  def create
    @bonu = Bonu.new(bonu_params)

    respond_to do |format|
      if @bonu.save
        format.html { redirect_to @bonu, notice: 'Bonus was successfully created.' }
        format.json { render :show, status: :created, location: @bonu }
      else
        format.html { render :new }
        format.json { render json: @bonu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bonus/1
  # PATCH/PUT /bonus/1.json
  def update
    respond_to do |format|
      if @bonu.update(bonu_params)
        format.html { redirect_to @bonu, notice: 'Bonus was successfully updated.' }
        format.json { render :show, status: :ok, location: @bonu }
      else
        format.html { render :edit }
        format.json { render json: @bonu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bonus/1
  # DELETE /bonus/1.json
  def destroy
    @bonu.destroy
    respond_to do |format|
      format.html { redirect_to bonus_url, notice: 'Bonus was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def status
  @bonu = Bonu.find(params[:id]) 
  redirect_to bonus_path if current_user.driver?   
  end

  def status_update
    @bonu = Bonu.find params[:id]
    respond_to do |format|
      if @bonu.update(bonu_params)
        format.html { redirect_to works_path, notice: 'Work status was successfully updated.' }
        format.json { redirect_to controller: :works, action: :index, status: :ok, location: :works }
      else
        format.html { render :status }
        format.json { render json: @bonu.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bonu
      @bonu = Bonu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bonu_params
      params.require(:bonu).permit(:amount, :reason, :dayt, :user_id, :tat)
    end
end
