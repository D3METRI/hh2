json.array!(@slocs) do |sloc|
  json.extract! sloc, :id, :officename, :address, :lat, :lng
  json.url sloc_url(sloc, format: :json)
end
