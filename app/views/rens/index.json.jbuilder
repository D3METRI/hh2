json.array!(@rens) do |ren|
  json.extract! ren, :id, :step_id, :user_id, :visit, :rentim
  json.url ren_url(ren, format: :json)
end
