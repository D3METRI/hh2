json.array!(@rowts) do |rowt|
  json.extract! rowt, :id, :name
  json.url rowt_url(rowt, format: :json)
end
