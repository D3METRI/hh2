json.array!(@lineitems) do |lineitem|
  json.extract! lineitem, :id, :invoice_id, :stat_id, :ren_id
  json.url lineitem_url(lineitem, format: :json)
end
