json.array!(@locations) do |location|
  json.extract! location, :id, :address, :lat, :lng, :step_id
  json.url location_url(location, format: :json)
end
