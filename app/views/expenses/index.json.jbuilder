json.array!(@expenses) do |expense|
  json.extract! expense, :id, :name, :amount, :user
  json.url expense_url(expense, format: :json)
end
