json.array!(@schedules) do |schedule|
  json.extract! schedule, :id, :day, :start_time, :user_id, :rowt_id
  json.url schedule_url(schedule, format: :json)
end
