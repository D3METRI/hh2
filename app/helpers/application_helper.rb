module ApplicationHelper

	def link_to_add_stepfields(name, f, association)
		new_object = f.object.send(association).klass.new
		id = new_object.object_id
		stepfields = f.fields_for(association, new_object, child_index: id) do |builder|
			render(association.to_s.singularize, f: builder)
		end
		link_to(name, '#', class: "add_stepfields", data: {id: id, stepfields: stepfields.gsub("\n", "")})
	end

	def link_to_add_fields(name, f, association)
	  new_object = f.object.class.reflect_on_association(association).klass.new
	  fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
	    render(association.to_s.singularize, :f => builder)
	  end
	  link_to(name, h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")"))
	end

	
  def bootstrap_class_for(flash_type)
    case flash_type
      when "success"
        "alert-success"   # Green
      when "error"
        "alert-danger"    # Red
      when "alert"
        "alert-warning"   # Yellow
      when "notice"
        "alert-info"      # Blue
      else
        flash_type.to_s
    end
  end

  def current_dispatch
  	current_user.userlevel ==  'dispatch'
  end

  def need_sloc
    current_user.sloc == nil
  end

  def current_admin
  	current_user.where(:userlevel => 'admin')
  end

end
