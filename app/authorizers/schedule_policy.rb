class SchedulePolicy < ApplicationPolicy
  def update?
    user.admin? or not record.published?
  end


  def destroy?
    user.admin?
  end
  
end