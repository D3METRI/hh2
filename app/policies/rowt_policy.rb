class RowtPolicy < ApplicationPolicy
	attr_reader :user, :model

  def initialize(user, model)
    @user = user
    @rowt = model
  end

  def new?
  	user.driver?
  end


  def create?
    user.driver?
  end

  def destroy?
    user.admin?
  end
end