class RenPolicy < ApplicationPolicy
	attr_reader :user, :record

  def initialize(user, model)
    @user = user
    @ren = model
  end


  def update?
    user.dispatch? || user.admin?
  end

    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      if user.admin? || user.dispatch?
        scope.all
      else
        scope.where(user: user)
      end
    end

end