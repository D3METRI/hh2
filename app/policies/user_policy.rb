class UserPolicy < ApplicationPolicy
	attr_reader :user, :model


  class Scope < Scope
    def resolve
      scope.where(:user_id => user.id)
    end
  end

  def initialize(user, model)
    @user = user
    @userr = model
  end

  def show?
  end  

end